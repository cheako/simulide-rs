// SimulIDE-rs - Electronic Circuit Simulator
// Copyright (C) 2019  Michael Mestnik <cheako+github_com@mikemestnik.net>
// This software is licensed under GPL-3 (see COPYING)

use std::cell::{Cell, RefCell};
use std::time::Instant;

use ash_tray::ash::prelude::VkResult;
use ash_tray::ash::vk;
use ash_tray::vk_helper::*;
use ash_tray::winit::{dpi::LogicalSize, window::WindowBuilder};
use ash_tray::winit::{
    event::*,
    event_loop::{ControlFlow, EventLoop, EventLoopWindowTarget},
    window::Window,
};

mod interface;

type UserEvent = ();

struct App {
    surface: SurfaceKHR,
    allocator: Allocator,
    window: Window,
    present_queue: Queue,
    submit_fence: Fence,
    command_buffer: CommandBuffer,
    command_buffer_reset: Cell<bool>,
    present_complete_semaphore: Semaphore,
    rendering_complete_semaphore: Semaphore,
    viewport: RefCell<Option<Viewport>>,
    imgui: ImGui,
}

fn main() {
    let event_loop = EventLoop::<UserEvent>::with_user_event();

    let window = WindowBuilder::new()
        .with_title("SimulIDE")
        /* .with_inner_size(LogicalSize::new(800_f64, 600_f64)) */
        .with_visible(false)
        .build(&event_loop)
        .unwrap();

    use ash_tray::ash::extensions::khr::Swapchain;
    use ash_tray::vk_make_version;
    use ash_tray::WindowExt;
    use std::ffi::CString;

    let app_name = CString::new("Simulide").unwrap();
    let appinfo = vk::ApplicationInfo::builder()
        .application_name(&app_name)
        .application_version(0)
        .engine_name(&app_name)
        .engine_version(0)
        .api_version(vk_make_version!(1, 0, 0));

    let extension_names_raw = window.get_required_extensions().unwrap();
    let create_info = vk::InstanceCreateInfo::builder()
        .application_info(&appinfo)
        .enabled_extension_names(&extension_names_raw);

    let instance = Instance::new_simple(&create_info).expect("Instance creation error");

    let surface = SurfaceKHR::new(&instance, &window, 0).expect("Surface creation error");
    let device_extension_names_raw = [Swapchain::name().as_ptr()];
    let features = vk::PhysicalDeviceFeatures::builder().shader_clip_distance(true);
    let priorities = [1.0];

    let create_info = [vk::DeviceQueueCreateInfo::builder()
        .queue_family_index(surface.queue_family_index)
        .queue_priorities(&priorities)
        .build()];

    let create_info = vk::DeviceCreateInfo::builder()
        .queue_create_infos(&create_info)
        .enabled_extension_names(&device_extension_names_raw)
        .enabled_features(&features);

    let device = Device::new(&instance, surface.physical_device, &create_info)
        .expect("Device creation error");

    let allocator = Allocator::new(&instance, surface.physical_device, &device)
        .expect("vk_mem: Allocator creation error");

    let present_queue = Queue::new(&device, surface.queue_family_index, 0);

    let create_info = vk::CommandPoolCreateInfo::builder()
        .flags(vk::CommandPoolCreateFlags::RESET_COMMAND_BUFFER)
        .queue_family_index(surface.queue_family_index);

    let command_pool = CommandPool::new(&device, &create_info).expect("CommandPool creation error");

    let create_info =
        vk::CommandBufferAllocateInfo::builder().level(vk::CommandBufferLevel::PRIMARY);

    let command_buffer =
        CommandBuffer::new(&command_pool, create_info).expect("CommandBuffer creation error");

    let submit_fence = Fence::new(&device, &Default::default()).expect("Create fence failed.");

    let present_complete_semaphore =
        Semaphore::new(&device, &Default::default()).expect("First Semaphore creation error");
    let rendering_complete_semaphore =
        Semaphore::new(&device, &Default::default()).expect("Second Semaphore creation error");

    let LogicalSize { width, height } = window.inner_size();

    let (width, height) = (width.round() as u32, height.round() as u32);
    let viewport = RefCell::new(Some(Viewport::new(&surface, &allocator, width, height)));

    use ash_tray::ash::version::DeviceV1_0;
    unsafe {
        device.reset_command_buffer(
            **command_buffer,
            ash_tray::ash::vk::CommandBufferResetFlags::RELEASE_RESOURCES,
        )
    }
    .expect("Reset command buffer failed.");

    let begin_info = ash_tray::ash::vk::CommandBufferBeginInfo::builder()
        .flags(ash_tray::ash::vk::CommandBufferUsageFlags::ONE_TIME_SUBMIT);

    unsafe { device.begin_command_buffer(**command_buffer, &begin_info) }
        .expect("Begin commandbuffer");

    let imgui = ImGui::new(
        &allocator,
        &command_buffer,
        &window,
        &viewport.borrow().as_ref().unwrap().render_pass,
    );

    window.set_visible(true);
    let app = App {
        surface,
        allocator,
        window,
        present_queue,
        submit_fence,
        command_buffer,
        command_buffer_reset: Cell::new(true),
        present_complete_semaphore,
        rendering_complete_semaphore,
        viewport,
        imgui,
    };
    event_loop.run(move |e, w, c| app.run(e, w, c));
}

pub struct Viewport {
    pub surface_resolution: vk::Extent2D,

    pub swapchain: SwapchainKHR,
    pub render_pass: RenderPass,
    pub next_images: Vec<(Framebuffer, ImageView)>,

    pub viewports: Vec<vk::Viewport>,
    pub scissors: Vec<vk::Rect2D>,
}

impl Viewport {
    pub(crate) fn new(
        surface: &SurfaceKHR,
        allocator: &Allocator,
        window_width: u32,
        window_height: u32,
    ) -> Self {
        let device = allocator.device.clone();
        let surface_capabilities = unsafe {
            surface
                .surface_loader
                .get_physical_device_surface_capabilities(surface.physical_device, ***surface)
        }
        .unwrap();
        let mut desired_image_count = surface_capabilities.min_image_count + 1;
        if surface_capabilities.max_image_count > 0
            && desired_image_count > surface_capabilities.max_image_count
        {
            desired_image_count = surface_capabilities.max_image_count;
        }
        let surface_resolution = match surface_capabilities.current_extent.width {
            std::u32::MAX => vk::Extent2D {
                width: window_width,
                height: window_height,
            },
            _ => surface_capabilities.current_extent,
        };
        let pre_transform = if surface_capabilities
            .supported_transforms
            .contains(vk::SurfaceTransformFlagsKHR::IDENTITY)
        {
            vk::SurfaceTransformFlagsKHR::IDENTITY
        } else {
            surface_capabilities.current_transform
        };
        let present_modes = unsafe {
            surface
                .surface_loader
                .get_physical_device_surface_present_modes(surface.physical_device, ***surface)
        }
        .unwrap();
        let present_mode = present_modes
            .iter()
            .cloned()
            .find(|&mode| mode == vk::PresentModeKHR::MAILBOX)
            .unwrap_or(vk::PresentModeKHR::FIFO);

        let create_info = vk::SwapchainCreateInfoKHR::builder()
            .min_image_count(desired_image_count)
            .image_color_space(surface.surface_format.color_space)
            .image_format(surface.surface_format.format)
            .image_extent(surface_resolution)
            .image_usage(vk::ImageUsageFlags::COLOR_ATTACHMENT)
            .image_sharing_mode(vk::SharingMode::EXCLUSIVE)
            .pre_transform(pre_transform)
            .composite_alpha(vk::CompositeAlphaFlagsKHR::OPAQUE)
            .present_mode(present_mode)
            .clipped(true)
            .image_array_layers(1);

        let swapchain =
            SwapchainKHR::new(&device, &surface, create_info).expect("Swapchain creation error");

        let present_image_views: Vec<ImageView> = swapchain
            .present_images
            .iter()
            .map(|image| {
                let create_info = vk::ImageViewCreateInfo::builder()
                    .view_type(vk::ImageViewType::TYPE_2D)
                    .format(surface.surface_format.format)
                    .subresource_range(vk::ImageSubresourceRange {
                        aspect_mask: vk::ImageAspectFlags::COLOR,
                        base_mip_level: 0,
                        level_count: 1,
                        base_array_layer: 0,
                        layer_count: 1,
                    });

                ImageView::new(&image, create_info).expect("Present Image View creation error")
            })
            .collect();

        let render_pass_attachments = [vk::AttachmentDescription {
            format: surface.surface_format.format,
            samples: vk::SampleCountFlags::TYPE_1,
            load_op: vk::AttachmentLoadOp::CLEAR,
            store_op: vk::AttachmentStoreOp::STORE,
            final_layout: vk::ImageLayout::PRESENT_SRC_KHR,
            ..Default::default()
        }];
        let color_attachment_refs = [vk::AttachmentReference {
            attachment: 0,
            layout: vk::ImageLayout::COLOR_ATTACHMENT_OPTIMAL,
        }];
        let dependencies = [vk::SubpassDependency {
            src_subpass: vk::SUBPASS_EXTERNAL,
            src_stage_mask: vk::PipelineStageFlags::COLOR_ATTACHMENT_OUTPUT,
            dst_access_mask: vk::AccessFlags::COLOR_ATTACHMENT_READ
                | vk::AccessFlags::COLOR_ATTACHMENT_WRITE,
            dst_stage_mask: vk::PipelineStageFlags::COLOR_ATTACHMENT_OUTPUT,
            ..Default::default()
        }];

        let subpasses = [vk::SubpassDescription::builder()
            .color_attachments(&color_attachment_refs)
            .pipeline_bind_point(vk::PipelineBindPoint::GRAPHICS)
            .build()];

        let create_info = vk::RenderPassCreateInfo::builder()
            .attachments(&render_pass_attachments)
            .subpasses(&subpasses)
            .dependencies(&dependencies);

        let render_pass =
            RenderPass::new(&device, &create_info).expect("Render Pass cretion error");

        let next_images = present_image_views
            .into_iter()
            .map(|present_image_view| {
                let framebuffer_attachments = [**present_image_view];
                let create_info = vk::FramebufferCreateInfo::builder()
                    .attachments(&framebuffer_attachments)
                    .width(surface_resolution.width)
                    .height(surface_resolution.height)
                    .layers(1);

                (
                    Framebuffer::new(&render_pass, create_info)
                        .expect("Framebuffer crreation error"),
                    present_image_view,
                )
            })
            .collect::<Vec<_>>();

        let viewports = vec![vk::Viewport {
            x: 0.0,
            y: 0.0,
            width: surface_resolution.width as f32,
            height: surface_resolution.height as f32,
            min_depth: 0.0,
            max_depth: 1.0,
        }];
        let scissors = vec![vk::Rect2D {
            offset: vk::Offset2D { x: 0, y: 0 },
            extent: surface_resolution,
        }];
        Self {
            surface_resolution,
            swapchain,
            render_pass,
            next_images,
            viewports,
            scissors,
        }
    }

    pub fn acquire_next_image(
        &self,
        timeout: u64,
        semaphore: vk::Semaphore,
        fence: vk::Fence,
    ) -> VkResult<(u32, bool)> {
        unsafe {
            self.swapchain.swapchain_loader.acquire_next_image(
                **self.swapchain,
                timeout,
                semaphore,
                fence,
            )
        }
    }

    pub fn queue_present(
        &self,
        queue: vk::Queue,
        create_info: vk::PresentInfoKHRBuilder,
    ) -> VkResult<bool> {
        let create_info = create_info.swapchains(std::slice::from_ref(&**self.swapchain));
        unsafe {
            self.swapchain
                .swapchain_loader
                .queue_present(queue, &create_info)
        }
    }
}

struct ImGui {
    imgui: RefCell<ImContext>,
    renderer: ash_tray::imguiash::Renderer,
    last_frame: Cell<Instant>,
    has_focus: Cell<bool>,
    current_cursor: Cell<Option<ash_tray::imgui::MouseCursor>>,
}

use ash_tray::imgui::{
    BackendFlags as ImBackendFlags, ClipboardBackend, Context as ImContext, ImStr, ImString, Io,
    Key,
};
use ash_tray::imguiash::Renderer;
use ash_tray::winit::{dpi::LogicalPosition, event::VirtualKeyCode};
use clipboard::{ClipboardContext, ClipboardProvider};

struct MyClipboardBackend(ClipboardContext);

impl Default for MyClipboardBackend {
    fn default() -> Self {
        Self(ClipboardProvider::new().unwrap())
    }
}

impl ClipboardBackend for MyClipboardBackend {
    fn get(&mut self) -> Option<ImString> {
        match self.0.get_contents() {
            Ok(o) => Some(ImString::new(o)),
            Err(o) => {
                println!("{:?}", o);
                None
            }
        }
    }

    fn set(&mut self, value: &ImStr) {
        self.0.set_contents(value.to_str().to_owned()).unwrap();
    }
}

extern "C" {
    fn igGetStyle() -> *mut std::ffi::c_void;
    fn igStyleColorsDark(dst: *mut std::ffi::c_void);
    fn ImGuiIO_AddInputCharacter(self_: *mut Io, c: std::os::raw::c_uint);
}

impl ImGui {
    fn new(
        allocator: &Allocator,
        command_buffer: &CommandBuffer,
        window: &Window,
        render_pass: &RenderPass,
    ) -> Self {
        let imgui = RefCell::new(ImContext::create());

        unsafe {
            igStyleColorsDark(igGetStyle());
        }

        imgui
            .borrow_mut()
            .set_clipboard_backend(Box::new(MyClipboardBackend::default()));

        let renderer = Renderer::new(
            &allocator.device,
            allocator,
            command_buffer,
            &mut imgui.borrow_mut(),
        )
        .unwrap();

        renderer.set_render_pass(&render_pass).unwrap();

        {
            let hidpi_factor = window.hidpi_factor().round();
            let logical_size = window.inner_size();

            let mut imgui = imgui.borrow_mut();
            let io = imgui.io_mut();
            if false {
                let logical_pos =
                    LogicalPosition::new(f64::from(io.mouse_pos[0]), f64::from(io.mouse_pos[1]));
                window.set_cursor_position(logical_pos).unwrap();
            }
            io.display_framebuffer_scale = [hidpi_factor as f32, hidpi_factor as f32];
            io.display_size = [logical_size.width as f32, logical_size.height as f32];
            io.backend_flags.insert(ImBackendFlags::HAS_MOUSE_CURSORS);
            io.backend_flags.insert(ImBackendFlags::HAS_SET_MOUSE_POS);
            io[Key::Tab] = VirtualKeyCode::Tab as _;
            io[Key::LeftArrow] = VirtualKeyCode::Left as _;
            io[Key::RightArrow] = VirtualKeyCode::Right as _;
            io[Key::UpArrow] = VirtualKeyCode::Up as _;
            io[Key::DownArrow] = VirtualKeyCode::Down as _;
            io[Key::PageUp] = VirtualKeyCode::PageUp as _;
            io[Key::PageDown] = VirtualKeyCode::PageDown as _;
            io[Key::Home] = VirtualKeyCode::Home as _;
            io[Key::End] = VirtualKeyCode::End as _;
            io[Key::Insert] = VirtualKeyCode::Insert as _;
            io[Key::Delete] = VirtualKeyCode::Delete as _;
            io[Key::Backspace] = VirtualKeyCode::Back as _;
            io[Key::Space] = VirtualKeyCode::Space as _;
            io[Key::Enter] = VirtualKeyCode::Return as _;
            io[Key::Escape] = VirtualKeyCode::Escape as _;
            io[Key::A] = VirtualKeyCode::A as _;
            io[Key::C] = VirtualKeyCode::C as _;
            io[Key::V] = VirtualKeyCode::V as _;
            io[Key::X] = VirtualKeyCode::X as _;
            io[Key::Y] = VirtualKeyCode::Y as _;
            io[Key::Z] = VirtualKeyCode::Z as _;

            imgui.set_platform_name(Some(ImString::from(format!(
                "imgui-simulide {}",
                env!("CARGO_PKG_VERSION")
            ))));
        }

        Self {
            imgui,
            renderer,
            last_frame: Cell::new(Instant::now()),
            has_focus: Default::default(),
            current_cursor: Default::default(),
        }
    }
}

impl App {
    fn run(
        &self,
        e: Event<UserEvent>,
        _window_target: &EventLoopWindowTarget<UserEvent>,
        c: &mut ControlFlow,
    ) {
        if !self.command_buffer_reset.get() {
            use ash_tray::ash::version::DeviceV1_0;
            unsafe {
                self.allocator.device.reset_command_buffer(
                    **self.command_buffer,
                    ash_tray::ash::vk::CommandBufferResetFlags::RELEASE_RESOURCES,
                )
            }
            .expect("Reset command buffer failed.");

            let begin_info = ash_tray::ash::vk::CommandBufferBeginInfo::builder()
                .flags(ash_tray::ash::vk::CommandBufferUsageFlags::ONE_TIME_SUBMIT);

            unsafe {
                self.allocator
                    .device
                    .begin_command_buffer(**self.command_buffer, &begin_info)
            }
            .expect("Begin commandbuffer");
            self.command_buffer_reset.set(true);
        }

        use ash_tray::winit::event::Event::*;

        let mut imgui = self.imgui.imgui.borrow_mut();
        let io = imgui.io_mut();
        match &e {
            WindowEvent { event, .. } => {
                use ash_tray::winit::event::{
                    ElementState::*, KeyboardInput as KeyInput, MouseScrollDelta::*, WindowEvent::*,
                };

                match event {
                    CloseRequested => *c = ControlFlow::Exit,
                    ReceivedCharacter(codepoint) => {
                        unsafe {
                            ImGuiIO_AddInputCharacter(io as *mut Io, *codepoint as u32);
                        };
                    }
                    Focused(x) => {
                        self.imgui.has_focus.set(*x);
                    }
                    KeyboardInput {
                        input:
                            KeyInput {
                                state,
                                virtual_keycode: Some(virtual_keycode),
                                modifiers,
                                ..
                            },
                        ..
                    } => {
                        io.keys_down[*virtual_keycode as usize] = *state == Pressed;
                        io.key_ctrl = modifiers.ctrl;
                        io.key_shift = modifiers.shift;
                        io.key_alt = modifiers.alt;
                        io.key_super = modifiers.logo;
                    }
                    CursorMoved {
                        position,
                        modifiers,
                        ..
                    } => {
                        io.mouse_pos = [position.x as f32, position.y as f32];
                        io.key_ctrl = modifiers.ctrl;
                        io.key_shift = modifiers.shift;
                        io.key_alt = modifiers.alt;
                        io.key_super = modifiers.logo;
                    }
                    MouseWheel {
                        delta: LineDelta(x, y),
                        modifiers,
                        ..
                    } => {
                        io.mouse_wheel_h += x;
                        io.mouse_wheel += y;
                        io.key_ctrl = modifiers.ctrl;
                        io.key_shift = modifiers.shift;
                        io.key_alt = modifiers.alt;
                        io.key_super = modifiers.logo;
                    }
                    MouseWheel {
                        delta: PixelDelta(LogicalPosition { x, y }),
                        modifiers,
                        ..
                    } => {
                        io.mouse_wheel_h = *x as f32;
                        io.mouse_wheel = *y as f32;
                        io.key_ctrl = modifiers.ctrl;
                        io.key_shift = modifiers.shift;
                        io.key_alt = modifiers.alt;
                        io.key_super = modifiers.logo;
                    }
                    MouseInput {
                        state,
                        button,
                        modifiers,
                        ..
                    } => {
                        io.mouse_down[{
                            use ash_tray::winit::event::MouseButton::*;
                            match button {
                                Left => 0,
                                Right => 1,
                                Middle => 2,
                                Other(x) => *x as usize,
                            }
                        }] = *state == Pressed;
                        io.key_ctrl = modifiers.ctrl;
                        io.key_shift = modifiers.shift;
                        io.key_alt = modifiers.alt;
                        io.key_super = modifiers.logo;
                    }
                    HiDpiFactorChanged(hidpi_factor) => {
                        io.display_framebuffer_scale = [*hidpi_factor as f32, *hidpi_factor as f32];
                    }
                    Resized(logical_size) => {
                        let surface_resolution =
                            self.viewport.borrow().as_ref().unwrap().surface_resolution;
                        if logical_size.width as u32 != surface_resolution.width
                            && logical_size.height as u32 != surface_resolution.height
                        {
                            std::mem::drop(self.viewport.replace(None));

                            self.viewport.replace(Some(Viewport::new(
                                &self.surface,
                                &self.allocator,
                                logical_size.width as _,
                                logical_size.height as _,
                            )));

                            self.imgui
                                .renderer
                                .set_render_pass(
                                    &self.viewport.borrow().as_ref().unwrap().render_pass,
                                )
                                .unwrap();

                            let hidpi_factor = self.window.hidpi_factor().round();
                            io.display_framebuffer_scale =
                                [hidpi_factor as f32, hidpi_factor as f32];
                            io.display_size =
                                [logical_size.width as f32, logical_size.height as f32];
                        }
                    }
                    RedrawRequested => {
                        use ash_tray::ash::version::DeviceV1_0;

                        self.imgui.renderer.begin_frame();

                        let present_index = unsafe {
                            let present_index =
                                match self.viewport.borrow().as_ref().unwrap().acquire_next_image(
                                    std::u64::MAX,
                                    **self.present_complete_semaphore,
                                    ash_tray::ash::vk::Fence::null(),
                                ) {
                                    Ok((p, _)) => p,
                                    Err(_) => return,
                                };

                            let clear_values = [vk::ClearValue {
                                color: vk::ClearColorValue {
                                    float32: [0.0, 0.0, 0.0, 0.0],
                                },
                            }];

                            let render_pass_begin_info = vk::RenderPassBeginInfo::builder()
                                .render_pass(**self.viewport.borrow().as_ref().unwrap().render_pass)
                                .framebuffer(
                                    **self.viewport.borrow().as_ref().unwrap().next_images
                                        [present_index as usize]
                                        .0,
                                )
                                .render_area(vk::Rect2D {
                                    offset: vk::Offset2D { x: 0, y: 0 },
                                    extent: self
                                        .viewport
                                        .borrow()
                                        .as_ref()
                                        .unwrap()
                                        .surface_resolution,
                                })
                                .clear_values(&clear_values);
                            self.allocator.device.cmd_begin_render_pass(
                                **self.command_buffer,
                                &render_pass_begin_info,
                                vk::SubpassContents::INLINE,
                            );

                            self.allocator.device.cmd_set_viewport(
                                **self.command_buffer,
                                0,
                                &self.viewport.borrow().as_ref().unwrap().viewports,
                            );
                            self.allocator.device.cmd_set_scissor(
                                **self.command_buffer,
                                0,
                                &self.viewport.borrow().as_ref().unwrap().scissors,
                            );
                            present_index
                        };

                        let temp = { io.update_delta_time(self.imgui.last_frame.get()) };
                        self.imgui.last_frame.set(temp);

                        if io.want_set_mouse_pos {
                            let logical_pos = LogicalPosition::new(
                                f64::from(io.mouse_pos[0]),
                                f64::from(io.mouse_pos[1]),
                            );
                            self.window.set_cursor_position(logical_pos).unwrap();
                        }

                        let io_mouse_cursor_change = !io
                            .config_flags
                            .contains(ash_tray::imgui::ConfigFlags::NO_MOUSE_CURSOR_CHANGE);
                        let io_mouse_draw_cursor = io.mouse_draw_cursor;

                        let ui = imgui.frame();

                        let mouse_cursor = ui.mouse_cursor();
                        if io_mouse_cursor_change && self.imgui.current_cursor.get() != mouse_cursor
                        {
                            use ash_tray::imgui;
                            use ash_tray::winit::window::CursorIcon as MouseCursor;

                            match mouse_cursor {
                                Some(mouse_cursor) if !io_mouse_draw_cursor => {
                                    self.window.set_cursor_visible(true);
                                    self.window.set_cursor_icon(match mouse_cursor {
                                        imgui::MouseCursor::Arrow => MouseCursor::Arrow,
                                        imgui::MouseCursor::TextInput => MouseCursor::Text,
                                        imgui::MouseCursor::ResizeAll => MouseCursor::Move,
                                        imgui::MouseCursor::ResizeNS => MouseCursor::NsResize,
                                        imgui::MouseCursor::ResizeEW => MouseCursor::EwResize,
                                        imgui::MouseCursor::ResizeNESW => MouseCursor::NeswResize,
                                        imgui::MouseCursor::ResizeNWSE => MouseCursor::NwseResize,
                                        imgui::MouseCursor::Hand => MouseCursor::Hand,
                                    });
                                }
                                _ => self.window.set_cursor_visible(false),
                            }
                            self.imgui.current_cursor.set(mouse_cursor);
                        }

                        interface::build(&ui);

                        self.imgui
                            .renderer
                            .render(ui, &self.command_buffer)
                            .unwrap();

                        unsafe {
                            self.allocator
                                .device
                                .cmd_end_render_pass(**self.command_buffer);
                            self.allocator
                                .device
                                .end_command_buffer(**self.command_buffer)
                                .expect("End commandbuffer");

                            let command_buffers = vec![**self.command_buffer];

                            let (wait_semaphores, signal_semaphores) = (
                                [**self.present_complete_semaphore],
                                [**self.rendering_complete_semaphore],
                            );
                            let submit_info = vk::SubmitInfo::builder()
                                .wait_semaphores(&wait_semaphores)
                                .wait_dst_stage_mask(&[
                                    vk::PipelineStageFlags::COLOR_ATTACHMENT_OUTPUT,
                                ])
                                .command_buffers(&command_buffers)
                                .signal_semaphores(&signal_semaphores);

                            /* std::process::exit(0); /* Exit or lockup. */ */

                            self.allocator
                                .device
                                .queue_submit(
                                    **self.present_queue,
                                    &[submit_info.build()],
                                    **self.submit_fence,
                                )
                                .expect("queue submit failed.");
                            self.allocator
                                .device
                                .wait_for_fences(&[**self.submit_fence], true, std::u64::MAX)
                                .expect("Wait for fence failed.");
                            self.allocator
                                .device
                                .reset_fences(&[**self.submit_fence])
                                .expect("Reset fence failed.");

                            let wait_semaphores = [**self.rendering_complete_semaphore];
                            let image_indices = [present_index];
                            let present_info = vk::PresentInfoKHR::builder()
                                .wait_semaphores(&wait_semaphores)
                                .image_indices(&image_indices);

                            self.viewport
                                .borrow()
                                .as_ref()
                                .unwrap()
                                .queue_present(**self.present_queue, present_info)
                                .unwrap();

                            self.command_buffer_reset.set(false);
                        };
                    }

                    _ => {}
                }
            }
            DeviceEvent { event, .. } => {
                use ash_tray::winit::event::{
                    DeviceEvent::*, ElementState::*, MouseScrollDelta::*,
                };

                match event {
                    Button {
                        button,
                        state: Released,
                    } => {
                        io.mouse_down[*button as usize] = false;
                    }
                    MouseWheel {
                        delta: LineDelta(x, y),
                    } => {
                        if self.imgui.has_focus.get() {
                            io.mouse_wheel_h += x;
                            io.mouse_wheel += y;
                        }
                    }
                    MouseWheel {
                        delta: PixelDelta(LogicalPosition { x, y }),
                    } => {
                        if self.imgui.has_focus.get() {
                            io.mouse_wheel_h = *x as f32;
                            io.mouse_wheel = *y as f32;
                        }
                    }
                    Key(KeyboardInput {
                        state: Released,
                        virtual_keycode: Some(virtual_keycode),
                        ..
                    }) => {
                        io.keys_down[*virtual_keycode as usize] = false;
                    }
                    _ => {}
                }
            }
            EventsCleared => {
                self.window.request_redraw();
            }
            _ => {}
        }
    }
}
