// SimulIDE-rs - Electronic Circuit Simulator
// Copyright (C) 2019  Michael Mestnik <cheako+github_com@mikemestnik.net>
// This software is licensed under GPL-3 (see COPYING)

pub fn build(ui: &ash_tray::imgui::Ui) {
    let mut demo = true;
    ui.show_demo_window(&mut demo);
}
